import 'package:clean_architecture/domain/entities/entities.dart';

import '../http/http.dart';

class RemoteAccountModel {
  final String accessToken;

  RemoteAccountModel(this.accessToken);

  factory RemoteAccountModel.fromMap(Map<String, dynamic> map) {
    if (map.containsKey('accessToken')) {
      return RemoteAccountModel(map['accessToken']);
    }
    throw HttpError.invalidData;
  }

  AccountEntity toEntity() => AccountEntity(accessToken);
}
