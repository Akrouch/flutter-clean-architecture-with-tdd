import 'dart:convert';

import 'package:http/http.dart';

import '../../data/http/http.dart';

class HttpAdapter implements HttpClient {
  final Client client;

  HttpAdapter(this.client);

  @override
  Future<Map<String, dynamic>?> request({
    required String url,
    required String method,
    Map<String, dynamic>? body,
  }) async {
    final headers = {
      'content-type': 'application/json',
      'accept': 'application/json',
    };

    var jsonBody = body != null ? json.encode(body) : null;
    var response = Response('', 500);
    try {
      if (method == 'post') {
        response = await client.post(
          Uri.parse(url),
          headers: headers,
          body: jsonBody,
        );
      }
    } catch (_) {
      throw HttpError.serverError;
    }

    return _handleResponse(response);
  }

  Map<String, dynamic>? _handleResponse(Response response) {
    if (response.statusCode == 200) {
      return response.body.isEmpty ? null : json.decode(response.body);
    } else if (response.statusCode == 400) {
      throw HttpError.badRequest;
    } else if (response.statusCode == 401) {
      throw HttpError.unauthorized;
    } else if (response.statusCode == 403) {
      throw HttpError.forbidden;
    } else if (response.statusCode == 404) {
      throw HttpError.notFound;
    } else if (response.statusCode == 500) {
      throw HttpError.serverError;
    }
    return null; //204
  }
}
