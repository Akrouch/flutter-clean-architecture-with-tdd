import 'package:clean_architecture/data/http/http.dart';
import 'package:clean_architecture/infra/http/http.dart';
import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mocktail/mocktail.dart';

class ClientSpy extends Mock implements Client {}

void main() {
  late ClientSpy client;
  late HttpAdapter sut;
  late String url;

  setUp(() {
    client = ClientSpy();
    sut = HttpAdapter(client);
    url = faker.internet.httpUrl();
  });

  group('shared', () {
    test('Should throw ServerError if invalid method is provided', () async {
      //execute the behaviour
      var future = sut.request(url: url, method: 'invalid_method');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.serverError));
    });
  });
  group('post', () {
    test('Should call post with correct url, body and any headers set',
        () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('{"any_key":"any_value"}', 200));

      //execute the behaviour
      await sut
          .request(url: url, method: 'post', body: {'any_key': 'any_value'});

      //verify if the executed matches the expected
      verify(() => client.post(Uri.parse(url),
          headers: any(named: 'headers'), body: any(named: 'body')));
    });

    test('Should call post without body', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 200));

      //execute the behaviour
      await sut.request(url: url, method: 'post');
      //verify if the executed matches the expected
      verify(() => client.post(Uri.parse(url),
          body: any(named: 'body'), headers: any(named: 'headers')));
    });

    test('Should return data if post returns 200 with data', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('{"any_key":"any_value"}', 200));

      //execute the behaviour
      final response = await sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(response, {'any_key': 'any_value'});
    });

    test('Should return null if post returns 200 with no data', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 200));

      //execute the behaviour
      var response = await sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(response, null);
    });

    test('Should return null if post returns 204', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 204));

      //execute the behaviour
      var response = await sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(response, null);
    });

    test('Should return null if post returns 204 even with data', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('{"any_key":"any_value"}', 204));

      //execute the behaviour
      var response = await sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(response, null);
    });

    test('Should return BadRequestError if post returns 400', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 400));

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.badRequest));
    });

    test('Should return Unauthorized if post returns 401', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 401));

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.unauthorized));
    });

    test('Should return Forbidden if post returns 403', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 403));

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.forbidden));
    });

    test('Should return NotFound if post returns 404', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 404));

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.notFound));
    });

    test('Should return BadRequestError if post returns 500', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async => Response('', 500));

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');
      //verify if the executed matches the expected

      expect(future, throwsA(HttpError.serverError));
    });

    test('Should throw ServerError if post throws unknown error', () async {
      //set the expected behaviour of a post from clientSpy
      when(() => client.post(Uri.parse(url),
          body: any(named: 'body'),
          headers: any(named: 'headers'))).thenThrow(Exception());

      //execute the behaviour
      var future = sut.request(url: url, method: 'post');

      //verify if the executed matches the expected
      expect(future, throwsA(HttpError.serverError));
    });
  });
}
