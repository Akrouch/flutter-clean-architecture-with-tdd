import 'package:clean_architecture/data/http/http.dart';
import 'package:clean_architecture/data/usecases/usecases.dart';
import 'package:clean_architecture/domain/helpers/helpers.dart';
import 'package:clean_architecture/domain/usecases/usecases.dart';
import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';
// import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class HttpClientSpy extends Mock implements HttpClient {}

void main() {
  late HttpClientSpy httpClient;
  // sut -> system under test
  late RemoteAuthentication sut;
  late String url;
  late AuthenticationParams params;

  setUp(() {
    //1. arrange
    httpClient = HttpClientSpy();
    url = faker.internet.httpUrl();
    sut =
        RemoteAuthentication(httpClient: httpClient, url: url, method: 'post');
    params = AuthenticationParams(
      email: faker.internet.email(),
      secret: faker.internet.password(),
    );
  });
  test(
    'Should call HttpClient with correct values',
    () async {
      when(() =>
          httpClient.request(
              url: any(named: 'url', that: isNotNull),
              method: any(named: 'method', that: isNotNull),
              body: any(named: 'body'))).thenAnswer((_) async => {
            'accessToken': faker.guid.guid(),
            'name': faker.person.name(),
          });
      //2. act
      await sut.auth(params);
      //3. assert
      verify(
        () => httpClient.request(
          url: url,
          method: 'post',
          body: {'email': params.email, 'password': params.secret},
        ),
      );
    },
  );

  test(
    'Should throw UnexpectedError if HttpClient returns 400',
    () async {
      when(() => httpClient.request(
              url: any(named: 'url', that: isNotNull),
              method: any(named: 'method', that: isNotNull),
              body: any(named: 'body'))) //forcando dar pau
          .thenThrow(HttpError.badRequest);

      //2. act
      var future = sut.auth(params);

      //3. assert
      expect(future, throwsA(DomainError.unexpected));
    },
  );

  test(
    'Should throw UnexpectedError if HttpClient returns 404',
    () async {
      when(() => httpClient.request(
          url: any(named: 'url', that: isNotNull),
          method: any(named: 'method', that: isNotNull),
          body: any(named: 'body'))).thenThrow(HttpError.notFound);

      //2. act
      var future = sut.auth(params);

      //3. assert
      expect(future, throwsA(DomainError.unexpected));
    },
  );

  test(
    'Should throw UnexpectedError if HttpClient returns 500',
    () async {
      when(() => httpClient.request(
          url: any(named: 'url', that: isNotNull),
          method: any(named: 'method', that: isNotNull),
          body: any(named: 'body'))).thenThrow(HttpError.serverError);

      //2. act
      var future = sut.auth(params);

      //3. assert
      expect(future, throwsA(DomainError.unexpected));
    },
  );

  test(
    'Should throw InvalidCredentialsError if HttpClient returns 401',
    () async {
      when(() => httpClient.request(
              url: any(named: 'url', that: isNotNull),
              method: any(named: 'method', that: isNotNull),
              body: any(named: 'body'))) //forcando dar pau
          .thenThrow(HttpError.unauthorized);

      //2. act
      var future = sut.auth(params);

      //3. assert
      expect(future, throwsA(DomainError.invalidCredentials));
    },
  );

  test(
    'Should return an Account if HttpClient returns 200',
    () async {
      final accessToken = faker.guid.guid();
      when(() =>
          httpClient.request(
              url: any(named: 'url', that: isNotNull),
              method: any(named: 'method', that: isNotNull),
              body: any(named: 'body'))).thenAnswer((_) async => {
            'accessToken': accessToken,
            'name': faker.person.name(),
          });

      //2. act
      var account = await sut.auth(params);
      //3. assert
      expect(
        account.token,
        accessToken,
      );
    },
  );

  test(
    'Should throw UnexpectedError if HttpClient returns 200 with invalid data',
    () async {
      when(() => httpClient.request(
              url: any(named: 'url', that: isNotNull),
              method: any(named: 'method', that: isNotNull),
              body: any(named: 'body')))
          .thenAnswer((_) async => {'invalid_key': 'invalid_value'});

      //2. act
      final future = sut.auth(params);

      expect(future, throwsA(DomainError.unexpected));
    },
  );
}
